Basado en: 
  http://www.django-rest-framework.org/tutorial/quickstart/
  http://www.django-rest-framework.org/tutorial/1-serialization/
git clone /path/to/this/proj django-rest-tutorial
cd django-rest-tutorial
python -m venv env
source env/bin/activate
pip install -r requeriments.txt
cd tutorial

Para ejecutar y seguir el projecto paso a paso, la instruccion general es:  
Mover hacia el paso en particular: git checkout step{n}, donde {n} es 0, 1, etc.
Revisar el codigo, y comparar novedad del paso actual respecto al anterior: git diff step{n-1} step{n}
Ejecutar el programas: python manage.py runserver
Verificar resultado: http://localhost:8000/

Ejemplo para revisar los primeros pasos:
### Initial: After django-admin startproject tutorial
git checkout step0
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver
#Testing:
http://localhost:8000/
http://localhost:8000/admin/

Nota: hay un commit intermedio entre step0 y step1, que es resutado de django-admin startapp quickstart
  que inicialmente no hace nada (vista vacia, y sin enlazar algun url). Para saber mas de ese commit:
nextCommit=$(git log  HEAD..master --oneline | tail -n 1 | cut -d' ' -f1)
#Or: nextCommit=$(git log  HEAD..master --oneline | tail -n 1 | cut -c1-7)
git diff HEAD $nextCommit 

### Quickstart
git checkout step1
python manage.py runserver

#Testing:
  curl -H 'Accept: Application/json; indent=4' -u admin:adminpass123 -i http://127.0.0.1:8000/users/
  http -a admin:adminpass123 http://127.0.0.1:8000/users/
Y deberia mostrar algo como:
HTTP/1.0 200 OK
Allow: GET, POST, HEAD, OPTIONS
Content-Type: application/json
Date: Thu, 30 Jun 2016 05:23:19 GMT
Server: WSGIServer/0.2 CPython/3.5.1
Vary: Accept, Cookie
X-Frame-Options: SAMEORIGIN

{
    "count": 1, 
    "next": null, 
    "previous": null, 
    "results": [
        {
            "email": "admin@test.com", 
            "groups": [], 
            "url": "http://127.0.0.1:8000/users/1/", 
            "username": "admin"
        }
    ]
}

En el browser: http://127.0.0.1:8000/users/
Error? Logue. El link del campo url funciona?
En el browser: http://127.0.0.1:8000/
Los links para users y groups funciona? Browsable api.
Fijese que en urls.py del proyecto include(router.urls) y no include(quickstart.urls)
Otras diferencias?

Para explicacion detallada de los otros pasos y algunas teorias, revisar DjangoRestFramework.txt
